<?php

/**
 * @file
 * Award Management funcitons and forms
 */

/**
 * include common functions
 */
module_load_include('inc', 'clantastic', '/includes/custom_functions');


/**
 * Delete an award definition.
 *
 * @params $cwid
 *   Award id to be editted.
 */
function clantastic_award_remove($cwid) {
  clantastic_remove_award($cwid);
  drupal_set_message(t('Award Definition Deleted'));
  drupal_goto('clantastic/member/award');
}


/**
 * Edit award definition.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 * @params $cwid
 *   Award id to be editted.
 */
function clantastic_award_edit($form, &$form_state, $cwid) {
  // Get memeber details.
  $award = clantastic_get_award_detail($cwid);

  // Hidden field for CID.
  $form['cwid'] = array(
    '#type' => 'hidden',
    '#title' => t('ID'),
    '#value' => $award->cwid,
  );

  // Section Header.
  $form['section_summary'] = array(
    '#type' => 'item',
    '#description' => t('Press the Save button to commit changes or the Delete link to delete the definition.'),
  );

  // Name.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#description' => t("The award or decoration's name."),
    '#default_value' => $award->title,
  );

  // Status.
  $form['is_active'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => array(0 => t('Inactive'), 1 => t('Active')),
    '#default_value' => $award->is_active,
  );

  // Prioirity.
  $form['priority'] = array(
    '#type' => 'textfield',
    '#title' => t('Priority'),
    '#size' => 10,
    '#default_value' => $award->priority,
    '#required' => TRUE,
    '#description' => t('A numeric value representing the presentation order, lowest number to highest.'),
  );

  // Description.
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#rows' => 2,
    '#default_value' => $award->description,
    '#description' => t("Enter a description for this award (i.e. 'A service award which recognizes those service members who have performed military tours of duty outside the borders.')"),
  );

  // Section Header.
  $form['section_title'] = array(
    '#type' => 'item',
    '#title' => t('Image'),
    '#description' => t('Upload a new image.'),
  );

  // Current Image.
  $form['image_markup'] = array(
    '#markup' => clantastic_create_member_image($award),
  );

  $form['file'] = array(
    '#name' => 'files[file]',
    '#type' => 'file',
    '#default_value' => $award->fid,
    '#upload_location' => 'public://clantastic/images/',
  );

  $form['#prefix'] = '<table><tr class="clantastic-side-by-side">';
  $form['#suffix'] = '</tr></table>';

  // Section Header.
  $form['section_actions'] = array(
    '#type' => 'item',
    '#title' => t('Actions'),
  );

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#prefix' => '<td width="20px">',
    '#suffix' => '</td>',
  );

  $form['delete'] = array(
    '#markup' => l(t('Delete'), 'clantastic/award/remove/' . $award->cwid),
    '#prefix' => '<td style="text-decoration:none">',
    '#suffix' => '</td>',
  );

  return $form;
}


/**
 * Validate award definition updates.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 */
function clantastic_award_edit_validate($form, &$form_state) {
  $file = file_save_upload('file', array(
    'file_validate_is_image' => array(),
    'file_validate_extensions' => array('png gif jpg jpeg'),
  ));

  // If the file passed validation.
  if ($file) {
    $file->status = 1;
    // Move the file, into the Drupal file system.
    $file_directory = 'public://clantastic/images';
    if ($file = file_move($file, $file_directory)) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t("Failed to write the uploaded file the site's file folder."));
    }
  }
  else {
    $form_state['storage']['file'] = $file;
  }
}


/**
 * Commit changes to award definition.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 */
function clantastic_award_edit_submit($form, &$form_state) {
  $value_change = FALSE;
  $values = $form_state['values'];
  $storage = $form_state['storage'];
  $value_change = clantastic_add_award($values, $storage, 'cwid');
  if ($value_change) {
    drupal_set_message(t('Award Updated'));
  }
  drupal_goto('clantastic/member/award');
}


/**
 * Form for assigning an award definition to a clan member.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 */
function clantastic_award_assign($form, &$form_state) {
  $form = array();

  $value_award_dropdown = isset($form_state['values']['dropdown_awards']) ? $form_state['values']['dropdown_awards'] : 1;
  $header = clantastic_get_tableselect_header_member_definition();

  $form['award'] = array(
    '#title' => t('Set Rank'),
    '#type' => 'fieldset',
    '#description' => t('Select an award, comment, the member to which apply, and the Mass Assign button.'),
  );

  $form['award']['dropdown_awards'] = array(
    '#type' => 'select',
    '#title' => t('Award & Decordation'),
    '#options' => clantastic_get_award_options(),
    '#default_value' => $value_award_dropdown,
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'clantastic_award_assign_ajax_callback',
      'wrapper' => 'tableselect_members_replace',
      'effect' => 'fade',
    ),
  );

  $form['award']['award_comment'] = array(
    '#type' => 'textarea',
    '#title' => t('Comments'),
    '#description' => t('(Optional) Enter a comment describing the nature of the award or decoration.'),
    '#rows' => 2,
  );

  // Section Header.
  $form['award']['section_title'] = array(
    '#type' => 'item',
    '#title' => t('Awards & Decorations Assignment'),
  );

  unset($form_state['input']['tableselect_members']);
  $form['award']['tableselect_members'] = array(
    '#multiple' => TRUE,
    '#type' => 'tableselect',
    '#header' => $header,
    '#prefix' => '<div id="tableselect_members_replace" style="width: 100%">',
    '#suffix' => '</div>',
    '#empty' => t('No clan members found'),
    '#options' => clantastic_award_assign_tableselect_options($value_award_dropdown),
    '#default_value' => clantastic_award_assign_options($value_award_dropdown),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Mass Assign'));
  $form['#submit'][] = 'clantastic_award_assign_submit';

  return $form;
}


/**
 * Assign or revoke an award or decoration.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 */
function clantastic_award_assign_submit($form, $form_state) {
  // Get form elements.
  $award_id = $form_state['values']['dropdown_awards'];
  $comments = $form_state['values']['award_comment'];
  $members = $form_state['values']['tableselect_members'];

  // Cycle through list to determine whether an award applied or revoked.
  foreach ($members as $cid_key => $cid) {
    // Selected to assign award ($cid != 0) and doesn't already.
    if ($cid != 0 && !clantastic_is_assigned($cid, $award_id)) {
      clantastic_assign_award($cid, $award_id, $comments);
    }
    elseif ($cid == 0 && clantastic_is_assigned($cid_key, $award_id)) {
      // If the award has been revoked.
      clantastic_remove_member_award($cid_key, $award_id);
    }
  }
}


/**
 * Boolean value check for award having already been applied to a member.
 *
 * @params $cid
 *   Clan member id to be evaluated.
 * @params $cwid
 *   Award id to be evaluated.
 *
 * @return Boolean.
 *   True if this award has been assigned.
 *   False if this award is not assigned.
 */
function clantastic_is_assigned($cid, $cwid) {
  $assigned = FALSE;
  $result = db_select('clantastic_award_clantastic_member', 'cacm')
    ->fields('cacm', array('cwid', 'cid'))
    ->condition('cwid', $cwid, '=')
    ->condition('cid', $cid, '=')
    ->execute();
  if ($result->rowCount() > 0) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}


/**
 * Insert a relationship between an award definition and a member.
 *
 * @params $cid
 *   Clan member id to be awarded.
 * @params $award_id
 *   Award id to be assigned.
 * @params $comments
 *    String comment added by user.
 */
function clantastic_assign_award($cid, $award_id, $comments) {
  $table = 'clantastic_award_clantastic_member';
  $record = new stdClass();
  $record->cwid = $award_id;
  $record->cid = $cid;
  $record->comments = $comments;
  drupal_write_record($table, $record);
}


/**
 * Assemble and print a table of defined awards and decorations.
 *
 *
 * @return Object.
 *   A form object.
 */
function clantastic_award_list() {
  $form = array();

  $header = clantastic_get_tableselect_header_image_definition();
  $awards = clantastic_get_award_definitions();

  $rows = array();

  foreach ($awards as $award) {
    $variables = clantastic_get_image_variables($award);
    $rows[] = array(
      'data' => array(
        l($award->title, 'clantastic/award/edit/' . $award->cwid),
        $award->priority,
        theme_image($variables),
        $award->description,),
    );
  }

  $form['table'] = array(
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No Award definitions found'),
  );

  return $form;
}


/**
 * Assemble and print a table of defined awards and decorations.
 *
 * @return Object.
 *   A form object.
 */
function clantastic_award_add() {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#description' => t("The award or decoration's name."),
  );

  $form['priority'] = array(
    '#type' => 'textfield',
    '#title' => t('Priority'),
    '#size' => 10,
    '#default_value' => 100,
    '#required' => TRUE,
    '#description' => t("The award or decoration's name."),
  );

  $description = "Enter a description for this award (i.e. 'A service award which recognizes ";
  $description += "those service members who have performed military tours of duty outside ";
  $description += "the borders.')";
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#required' => TRUE,
    '#description' => t($description),
  );

  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('Upload'),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['add'] = array('#type' => 'submit', '#value' => t('Add'));
  $form['#add'][] = 'clantastic_award_add_submit';

  return $form;
}


/**
 * Validate and process the input forms submitted image file.
 *
 * @params $form
 *   Original input form.
 * @params $form_state
 *   Input form.
 */
function clantastic_award_add_validate($form, &$form_state) {
  $file = file_save_upload('file', array(
    'file_validate_is_image' => array(),
    'file_validate_extensions' => array('png gif jpg jpeg'),
   ));

  // If the file passed validation.
  if ($file) {
    // Move the file, into the Drupal file system.
    $file->status = 1;
    $file_directory = 'public://clantastic/images';
    if ($file = file_move($file, $file_directory)) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t("Failed to write the uploaded file the site's file folder."));
    }
  }
  else {
    form_set_error('file', t('No file was uploaded.'));
  }
}


/**
 * Commit award definition.
 *
 * @params $form_state
 *   Input form.
 * @params $form_name
 *    Source input form object's name.
 */
function clantastic_award_add_submit($form, &$form_state) {
  $values = $form_state['values'];
  $storage = $form_state['storage'];

  clantastic_add_award($values, $storage);
  drupal_set_message(t('Award Created'));
  drupal_goto('clantastic/member/award');
}


/**
 * Insert an award row.
 *
 * @params $values
 *   Values object from $form_state.
 * @params $storage
 *   Reference to image id.
 * @params $primary_keys
 *    DB table key value.
 *
 * @return Boolean.
 *   True on successful updates.
 */
function clantastic_add_award($values, $storage, $primary_keys = NULL) {
  $table = 'clantastic_award';
  $record = new stdClass();
  $record->title = $values['title'];
  $record->priority = $values['priority'];
  $record->description = $values['description'];
  $record->created_at = time();

  // On a create action now cwid will be avialable (to be assigned by the db).
  if (isset($values['cwid'])) {
    $record->cwid = $values['cwid'];
  }

  // Update fid only if it is set.
  $is_set = isset($storage['file']);
  if ($is_set) {
    $record->fid = $storage['file']->fid;
  }

  // Update is_active if it is set.
  if (isset($values['is_active'])) {
    $record->is_active = $values['is_active'];
  }
  else {
    $record->is_active = 1;
  }

  if ($primary_keys) {
    $cwid = drupal_write_record($table, $record, $primary_keys);
  }
  else {
    $cwid = drupal_write_record($table, $record);
  }
  drupal_goto('clantastic/member/award');
}


/**
 * Create a row header.
 *
 * @return Array.
 *   An array of header label information.
 */
function clantastic_get_tableselect_header_member_definition() {
  $header = array(
    'name' => t('name'),
    'rank' => t('Current Rank'),
    'awards' => t('Awards'),
  );
  return $header;
}

/**
 * Create a row header.
 *
 * @return Array.
 *   An array of header label information.
 */
function clantastic_get_tableselect_header_image_definition() {
  $header = array(
    array('data' => t('Title'), 'width' => '20%'),
    array('data' => t('Priority')),
    array('data' => t('Image')),
    array('data' => t('Description')),
  );
  return $header;
}


/**
 * Return a list of all award definitions.
 *
 * @return Array.
 *   An array of award definition objects.
 */
function clantastic_get_award_definitions() {
  $query = db_select('clantastic_award', 'cw');
  $query->leftJoin('file_managed', 'fm', 'fm.fid = cw.fid');
  $awards = $query
  ->extend('TableSort')
  ->fields('cw', array('cwid', 'title', 'fid', 'description', 'priority'))
  ->fields('fm', array('filename', 'uri'))
  ->orderBy('title', 'ASC')
  ->execute();

  return $awards;
}


/**
 * Create an object of image parts.
 *
 * @params $image
 *   An image definition object.
 *
 * @return Array.
 *   An array containing a formated image object.
 */
function clantastic_get_image_variables($image) {
  $image_variables = array(
    'path' => clantastic_issetor($image->uri, 'public://clantastic/images/r0.png'),
    'alt' => $image->title,
    'title' => $image->title,
    'attributes' => array('class' => 'some-img', 'id' => 'my-img'),
  );

  if (!isset($image_variables)) {
    return array();
  }
  else {
    return $image_variables;
  }
}


/**
 * Ajax control to refresh table view.
 *
 * @params $field_name
 *   A string value representing the column name to return.
 * @params $form_state
 *   Input form.
 * @params $form_name
 *    Source input form object's name.
 *
 * @return Object.
 *   A form object.
 */
function clantastic_award_assign_ajax_callback($form, $form_state) {
  return $form['award']['tableselect_members'];
}


/**
 * Get an array of award and decoration names.
 *
 * @return Array
 *   An array of objects.
 */
function clantastic_get_award_options() {
  $query = db_query('SELECT cw.cwid, cw.title FROM {clantastic_award} cw where is_active = 1 ORDER BY cw.title');
  $awards = $query->fetchAllKeyed();

  if (!isset($awards)) {
    return array();
  }
  else {
    return $awards;
  }
}


/**
 * Get an array of award and decoration names associated with  clan members.
 *
 * @return Array.
 *   An array of objects.
 */
function clantastic_award_assign_tableselect_options($key = '') {
  $members = clantastic_get_member_list();
  foreach ($members as $member_index => $member) {
    $options[$member->cid] = array(
      'name' => $member->name,
      'rank' => $member->is_active ? $member->title : '',
      'awards' => clantastic_awards($member),
    );
  }

  if (isset($options)) {
    return $options;
  }
  else {
    return array();
  }
}


/**
 * Get an array of award and decoration names associated with  clan members.
 *
 * @return Array.
 *   An array of objects.
 */
function clantastic_award_assign_options($key = '') {
  $members = clantastic_get_member_list();
  $default_value = array();
  foreach ($members as $member) {
    $awards = clantastic_get_assigned_awards_list($member);
    $default_value[$member->cid] = 0;
    foreach ($awards as $award) {
      if ($key == $award->cwid) {
        $default_value[$member->cid] = 1;
        continue;
      }
    }
  }
  return $default_value;
}
