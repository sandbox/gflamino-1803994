<?php

/**
 * @file
 * Rank releated functions and forms
 */


/**
 * Include common functions.
 */
module_load_include('inc', 'clantastic', 'clantastic.admin');
module_load_include('inc', 'clantastic', '/includes/custom_functions');


/**
 * Remove a rank definition.
 */
function clantastic_rank_remove($crid) {
  // Get default rank.
  $default_rank_id = clantastic_fetch_config_field('default_rank_id');
  if ($crid == $default_rank_id) {
    drupal_set_message(t('Cannot Delete Default Rank Definition.  Please change default') . ' ' . l(t('Rank'), 'admin/config/clantastic/settings') . ' ' . t('before attempting.'), 'warning');
  }
  else {
    clantastic_remove_award($crid, $default_rank_id);
    drupal_set_message(t('Rank Definition Deleted'));
  }
  drupal_goto('clantastic/member/rank');

}


/**
 * Display a rank definition record.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 * @params $crid
 *   Rank id.
 *
 * @return: Object
 *   A form object.
 */
function clantastic_rank_edit($form, &$form_state, $crid) {
  // Get memeber details.
  $rank = clantastic_get_rank_detail($crid);

  // Hidden field for CRID.
  $form['crid'] = array(
    '#type' => 'hidden',
    '#title' => t('ID'),
    '#value' => $rank->crid,
  );

  // Section Header.
  $form['section_summary'] = array(
    '#type' => 'item',
    '#description' => t('Press the Save button to commit changes or the Delete link to delete the definition.'),
  );

  // Name.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#description' => t("The rank's name."),
    '#default_value' => $rank->title,
  );

  // Status.
  $form['is_active'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => array(0 => t('Inactive'), 1 => t('Active')),
    '#default_value' => $rank->is_active,
  );

  // Description.
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#rows' => 2,
    '#default_value' => $rank->description,
    '#description' => t("Enter a description for this rank (i.e. 'A first lieutenant is a junior commissioned officer. It is just above the rank of second lieutenant and just below the rank of captain.')"),
  );

  // Prioirity.
  $form['priority'] = array(
    '#type' => 'textfield',
    '#title' => t('Priority'),
    '#size' => 10,
    '#default_value' => $rank->priority,
    '#required' => TRUE,
    '#description' => t('A numeric value representing the presentation order, lowest number to highest.'),
  );

  // Section Header.
  $form['section_title'] = array(
    '#type' => 'item',
    '#title' => t('Image'),
    '#description' => t('Upload a new image.'),
  );

  // Current Image.
  $form['image_markup'] = array(
    '#markup' => clantastic_create_member_image($rank),
  );

  $form['file'] = array(
    '#name' => 'files[file]',
    '#type' => 'file',
    '#default_value' => $rank->fid,
    '#upload_location' => 'public://clantastic/images/',
  );

  $form['#prefix'] = '<table><tr class="clantastic-side-by-side">';
  $form['#suffix'] = '</tr></table>';

  // Section Header.
  $form['section_actions'] = array(
    '#type' => 'item',
    '#title' => t('Actions'),
  );

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#prefix' => '<td width="20px">',
    '#suffix' => '</td>',
  );

  $form['delete'] = array(
    '#markup' => l(t('Delete'), 'clantastic/rank/remove/' . $rank->crid),
    '#prefix' => '<td style="text-decoration:none">',
    '#suffix' => '</td>',
  );

  return $form;
}


/**
 * Validate edits to rank definition.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 *
 * @return: Object
 *   A form object.
 */
function clantastic_rank_edit_validate($form, &$form_state) {
  $file = file_save_upload('file', array(
    'file_validate_is_image' => array(),
    'file_validate_extensions' => array('png gif jpg jpeg'),
  ));

  // If the file passed validation.
  if ($file) {
    $file->status = 1;
    // Move the file, into the Drupal file system.
    $file_directory = 'public://clantastic/images';
    if ($file = file_move($file, $file_directory)) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t("Failed to write the uploaded file the site's file folder."));
    }
  }
  else {
    $form_state['storage']['file'] = $file;
  }
}


/**
 * Update changes to rank definition.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 *
 * @return: Object
 *   A form object.
 */
function clantastic_rank_edit_submit($form, &$form_state) {
  $value_change = FALSE;
  $values = $form_state['values'];
  $storage = $form_state['storage'];

  $table = 'clantastic_rank';
  $record = new stdClass();
  $record->crid = $values['crid'];
  $record->title = $values['title'];
  $record->description = $values['description'];
  $record->created_at = time();
  $record->is_active = $values['is_active'];
  $record->priority = $values['priority'];
  if (isset($storage['file'])) {
    $record->fid = $storage['file']->fid;
  }

  $value_change = drupal_write_record($table, $record, 'crid');

  if ($value_change) {
    drupal_set_message(t('Rank Updated'));
  }
  drupal_goto('clantastic/member/rank');
}


/**
 * Display a list of members where rank may be mass assigned.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 *
 * @return: Object
 *   A form object.
 */
function clantastic_rank_assign($form, &$form_state) {
  $value_rank_dropdown = isset($form_state['values']['dropdown_ranks']) ? $form_state['values']['dropdown_ranks'] : clantastic_fetch_config_field('default_rank_id');

  $form['rank'] = array(
    '#title' => t('Set Rank'),
    '#type' => 'fieldset',
  );

  $form['rank']['dropdown_ranks'] = array(
    '#type' => 'select',
    '#title' => t('Rank'),
    '#options' => clantastic_get_rank_options(),
    '#default_value' => $value_rank_dropdown,
    '#ajax' => array(
      'event' => 'change',
      'callback' => 'clantastic_rank_assign_ajax_callback',
      'wrapper' => 'tableselect_members_replace',
      'effect' => 'fade',
    ),
  );

  $header = array(
    'name' => t('name'),
    'rank' => t('Current Rank'),
  );

  unset($form_state['input']['tableselect_members']);
  $form['rank']['tableselect_members'] = array(
    '#multiple' => TRUE,
    '#type' => 'tableselect',
    '#header' => $header,
    '#prefix' => '<div id="tableselect_members_replace" style="width: 50%">',
    '#suffix' => '</div>',
    '#empty' => t('No members found'),
    '#options' => clantastic_rank_assign_tableselect_options($value_rank_dropdown),
    '#default_value' => clantastic_rank_assign_options($value_rank_dropdown),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Mass Assign'));
  $form['#submit'][] = 'clantastic_rank_assign_submit';

  return $form;
}


/**
 * Process rank assign values.
 */
function clantastic_rank_assign_submit($form, $form_state) {
  $assign_rank_id = $form_state['values']['dropdown_ranks'];
  $clantastic_member_list = $form_state['values']['tableselect_members'];
  $default_rank_id = clantastic_fetch_config_field('default_rank_id');
  foreach ($clantastic_member_list as $cid_key => $cid) {
    if ($cid != 0) {
      clantastic_update_clantastic_rank_id_if_different($cid_key, $assign_rank_id, $default_rank_id);
    }
  }
}


/**
 * Update member row if different.
 */
function clantastic_update_clantastic_rank_id_if_different($cid, $new_rank_id, $default_rank_id) {
  $return_value = FALSE;
  $field_name = 'crid';
  $db_value = clantastic_fetch_clantastic_member_value($cid, $field_name);
  if ($db_value != $new_rank_id) {
    // Use default rank id if value is 0.
    if ($new_rank_id == 0) {
      $new_rank_id = $default_rank_id;
    }
    // Update row.
    $return_value = db_update('clantastic_member')
    ->fields(array($field_name => $new_rank_id))
    ->condition('uid', $cid, '=')
    ->execute();
  }
  return $return_value;
}


/**
 * Retrieve a clan members details row.
 */
function clantastic_fetch_clantastic_member_value($cid, $field_name) {
  $result = db_select('clantastic_member', 'c')
  ->fields('c', array($field_name))
  ->condition('c.uid', $cid, '=')
  ->execute();
  $field_value = $result->fetchField();

  return $field_value;
}


/**
 * Selects just the second dropdown to be returned for re-rendering.
 */
function clantastic_rank_assign_ajax_callback($form, $form_state) {
  return $form['rank']['tableselect_members'];
}


/**
 * Helper function to populate the second dropdown.
 */
function clantastic_rank_assign_tableselect_options($key = '') {
  $users = clantastic_get_member_list();
  foreach ($users as $member_index => $user) {
    $options[$user->uid] = array(
      'name' => $user->name,
      'rank' => $user->is_active ? $user->title : '',
    );
  }

  if (isset($options)) {
    return $options;
  }
  else {
    return array();
  }
}


/**
 * Build a list of ranks as they are assigned to members.
 */
function clantastic_rank_assign_options($key = '') {
  $users = clantastic_get_member_list();
  $default_value = array();
  foreach ($users as $user) {
    $user_crid = $user->crid;
    $default_value[$user->uid] = $user_crid == $key ? 1 : 0;
  }
  return $default_value;
}

/**
 * Display list of defined ranks.
 */
function clantastic_rank_list() {
  $form = array();

  $header = array(
    array('data' => t('Title'), 'field' => 'title'),
    array('data' => t('Rank'), 'field' => 'image'),
    array('data' => t('Description'), 'field' => 'description'),
  );

  $query = db_select('clantastic_rank', 'cr');
  $query->leftJoin('file_managed', 'fm', 'fm.fid = cr.fid');
  $ranks = $query
  ->extend('TableSort')
  ->fields('cr', array('crid', 'title', 'fid', 'description'))
  ->fields('fm', array('filename', 'uri'))
  ->orderBy('priority', 'DESC')
  ->execute();

  $rows = array();

  $default_rank_id = clantastic_fetch_config_field('default_rank_id');
  $default_rank_text = '';

  foreach ($ranks as $rank) {
    $variables = array(
      'path' => clantastic_issetor($rank->uri, 'public://clantastic/images/r0.png'),
      'alt' => $rank->title,
      'title' => $rank->title,
      'attributes' => array('class' => 'clantastic-rank-image', 'id' => 'my-img'),
    );

    $crid = $rank->crid;

    $crid == $default_rank_id ? $default_rank_text = ' *' : $default_rank_text = '';

    $rows[] = array(
      'data' => array(
        l($rank->title, 'clantastic/rank/edit/' . $crid) . $default_rank_text,
        theme_image($variables),
        $rank->description,
      ),
    );
  }

  $form['table'] = array(
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No users found'),
  );

  return $form;
}


/**
 * New rank definition form.
 */
function clantastic_rank_add() {
  $form['rank_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#description' => t("Enter the new rank title (i.e. 'Private', 'Major')"),
  );

  $form['rank_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#required' => TRUE,
    '#description' => t("Enter a description for this rank (i.e. 'Second Lieutenant is the normal entry-level rank for most commissioned officers.')"),
  );

  // Prioirity.
  $form['priority'] = array(
    '#type' => 'textfield',
    '#title' => t('Priority'),
    '#size' => 10,
    '#required' => TRUE,
    '#description' => t('A numeric value representing the presentation order, lowest number to highest.'),
  );

  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('Upload'),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['add'] = array('#type' => 'submit', '#value' => t('Add'));
  $form['#add'][] = 'clantastic_rank_add_submit';

  return $form;
}


/**
 * Validate the new rank defintion parameters.
 */
function clantastic_rank_add_validate($form, &$form_state) {
  $file = file_save_upload('file', array(
    'file_validate_is_image' => array(),
    'file_validate_extensions' => array('png gif jpg jpeg'),
  ));

  // If the file passed validation.
  if ($file) {
    $file->status = 1;
    // Move the file, into the Drupal file system.
    $file_directory = 'public://clantastic/images';
    if ($file = file_move($file, $file_directory)) {
      // Save the file for use in the submit handler.
      $form_state['storage']['file'] = $file;
    }
    else {
      form_set_error('file', t("Failed to write the uploaded file the site's file folder."));
    }
  }
  else {
    form_set_error('file', t('No file was uploaded.'));
  }
}


/**
 * Insert new rank definition row.
 */
function clantastic_rank_add_submit($form, &$form_state) {
  $values = $form_state['values'];
  $storage = $form_state['storage'];

  $table = 'clantastic_rank';
  $record = new stdClass();
  $record->title = $values['rank_name'];
  $record->description = $values['rank_description'];
  $record->created_at = time();
  $record->fid = $storage['file']->fid;
  $record->priority = $values['priority'];
  drupal_write_record($table, $record);

  drupal_set_message(t('Rank Created'));
  drupal_goto('clantastic/member/rank');
}
