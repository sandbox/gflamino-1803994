<?php

/**
 * @file
 * Helpers and Custom Functions.
 */

/**
 * Get award definition details.
 */
function clantastic_get_award_detail($cwid) {
  $result = db_query('SELECT cwid, title, description, fid, priority, is_active FROM {clantastic_award} ca WHERE cwid = :cwid', array(':cwid' => $cwid));
  $award = $result->fetchObject();
  if (!isset($award)) {
    return array();
  }
  else {
    return $award;
  }
}


/**
 * Get rank definition details.
 */
function clantastic_get_rank_detail($crid) {
  $result = db_query('SELECT crid, title, description, fid, is_active, priority FROM {clantastic_rank} cr WHERE crid = :crid', array(':crid' => $crid));
  $rank = $result->fetchObject();
  if (!isset($rank)) {
    return array();
  }
  else {
    return $rank;
  }
}


/**
 * Disassociate an award definition row from a clan member.
 */
function clantastic_remove_member_award($cid, $award_id) {
  db_delete('clantastic_award_clantastic_member')
    ->condition('cid', $cid, '=')
    ->condition('cwid', $award_id, '=')
    ->execute();
}


/**
 * Delete an award definition row.
 */
function clantastic_remove_award($award_id) {
  // Remove references from association table.
  db_delete('clantastic_award_clantastic_member')
    ->condition('cwid', $award_id, '=')
    ->execute();

  // Remove award defition.
  db_delete('clantastic_award')
    ->condition('cwid', $award_id, '=')
    ->execute();
}


/**
 * Disassociate a rank definition row from a clan member.
 */
function clantastic_remove_awards($rank_id, $default_rank_id) {
  db_update('clantastic_member')
    ->fields(array('crid' => $default_rank_id))
    ->condition('crid', $rank_id, '=')
    ->execute();
  db_delete('clantastic_rank')
    ->condition('crid', $rank_id, '=')
    ->execute();
}


/**
 * Disassociated all award definitions from a clan member.
 */
function clantastic_remove_member_award_by_cwcmid($cwcmid) {
  db_delete('clantastic_award_clantastic_member')
    ->condition('cwcmid', $cwcmid, '=')
    ->execute();
}


/**
 * Helper function to fetch the value contained in specified column.
 */
function clantastic_fetch_config_field($field_name) {
  $result = db_select('clantastic_config', 'cc')
    ->fields('cc', array($field_name))
    ->condition('cc.ccid', 1, '=')
    ->execute();
  $field_value = $result->fetchField();
  return $field_value;
}


/**
 * Helper function that compare two variables.
 */
function clantastic_issetor(&$variable, $or = NULL) {
  return $variable === NULL ? $or : $variable;
}


/**
 * Get an array of all clan members.
 */
function clantastic_get_member_list() {
  $query = db_select('clantastic_member', 'c');
  $query->leftJoin('clantastic_rank', 'cr', 'cr.crid = c.crid');
  $query->rightJoin('users', 'u', 'u.uid = c.uid');
  $query
    ->fields('u', array('name', 'mail', 'created', 'uid'))
    ->fields('c', array('cid', 'is_active'))
    ->fields('cr', array('crid', 'title', 'fid'))
    ->condition('c.is_active', 1, '=')
    ->orderBy('c.cid', 'ASC');

  $members = $query->execute();
  return $members;
}


/**
 * Return award image object.
 */
function clantastic_create_member_image($award) {

  $result = db_query("SELECT * FROM {file_managed} WHERE fid = :fid", array(':fid' => $award->fid));
  $local_image = $result->fetchObject();

  $variables = array();

  $variables = array(
    'path' => clantastic_issetor($local_image->uri, 'public://clantastic/images/a0.png'),
    'alt' => $award->title,
    'title' => $award->title,
    'attributes' => array('class' => 'some-img', 'id' => 'my-img'),
  );

  return theme_image($variables);
}


/**
 * Return all awards assigned to this member.
 */
function clantastic_get_assigned_awards_list($member) {
  $query = db_select('clantastic_member', 'c');
  $query->leftJoin('clantastic_award_clantastic_member', 'cacm', 'cacm.cid = c.cid');
  $result = $query
    ->fields('c', array('cid'))
    ->fields('cacm', array('cwcmid', 'cwid', 'comments'))
    ->condition('c.cid', $member->cid, '=')
    ->orderBy('c.cid', 'ASC')
    ->execute();

  $awards = $result->fetchAll();
  return $awards;
}

/**
 * Return all awards assigned to this member.
 */
function clantastic_awards($member) {
  $value = '';
  $query = db_select('clantastic_member', 'c');
  $query->leftJoin('clantastic_award_clantastic_member', 'cacm', 'cacm.cid = c.cid');
  $query->leftJoin('clantastic_award', 'ca', 'ca.cwid = cacm.cwid');
  $query
    ->fields('ca', array('title', 'description', 'fid', 'priority', 'cwid'))
    ->fields('cacm', array('comments'))
    ->condition('c.cid', $member->cid, '=')
    ->orderBy('priority', 'ASC');

  $awards = $query->execute();

  foreach ($awards as $award) {
    $value .= clantastic_create_member_image($award);
  }

  if (!isset($value)) {
    return '';
  }
  else {
    return $value;
  }
}


/**
 * Helper function to fetch the names of all defined ranks.
 */
function clantastic_get_rank_options() {
  $query = db_query('SELECT cr.crid, cr.title FROM {clantastic_rank} cr where is_active = 1 ORDER BY cr.title');
  $ranks = $query->fetchAllKeyed();

  if (!isset($ranks)) {
    return array();
  }
  else {
    return $ranks;
  }
}
