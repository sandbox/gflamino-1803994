<?php
/**
 * @file
 * On Install Functions
 */


/**
 * Schema.
 */
function clantastic_schema() {

  $schema['clantastic_config'] = array(
    'description' => 'The module configuration table',
    'fields' => array(
      'ccid' => array(
        'description' => 'The primary identifier for this configuration file',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'is_rank_displayed' => array(
        'description' => 'Boolean value for setting summary view',
        'type' => 'int',
        'size' => 'tiny',
        'default' => 1,
        'not null' => TRUE,),
      'is_fullname_displayed' => array(
        'description' => 'Boolean value for setting detail view',
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'not null' => TRUE,),
      'is_location_displayed' => array(
        'description' => 'Boolean value for setting detail view',
        'type' => 'int',
        'size' => 'tiny',
        'default' => 1,
        'not null' => TRUE),
      'is_active' => array(
        'description' => 'Boolean value for setting presense in module view',
        'type' => 'int',
        'size' => 'tiny',
        'default' => 1,
        'not null' => TRUE),
      'default_rank_id' => array(
        'description' => 'To be set by administrator.  A value that represents the ID of the clan rank definition to be assigned when one is not otherwise specified',
        'type' => 'int'),
      'updated_at' => array(
        'description' => 'Date when record was update.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE),
    ),
    'primary key' => array('ccid'),
  );

  $schema['clantastic_member'] = array(
    'description' => 'Definition of a unique clan members information',
    'fields' => array(
      'cid' => array(
        'description' => 'The primary identifier for a clan mamber',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'uid' => array(
        'description' => 'The users drupal id',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'crid' => array(
        'description' => 'The clan rank ID assigned to this member',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'is_active' => array(
        'description' => 'Boolean value for setting presense in module view',
        'type' => 'int',
        'size' => 'tiny',
        'default' => 1,
        'not null' => TRUE,),
      'membership_at' => array(
        'description' => 'Date clan member was accepted.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'created_at' => array(
        'description' => 'Date when record was created.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE,),
    ),
    'indexes' => array(
      'cid' => array('cid'),
      'uid' => array('uid'),
      'crid' => array('crid'),
    ),
    'primary key' => array('cid'),
  );


  $schema['clantastic_activity'] = array(
    'description' => 'The table where an activity is defined (ie. World of Tanks, MineCraft, etc.)',
    'fields' => array(
      'caid' => array(
        'description' => 'The primary identifier for a clan activity such as a specific game',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'title' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',),
      'description' => array(
        'type' => 'varchar',
        'length' => 512,
        'not null' => TRUE,
        'default' => '',),
      'is_active' => array(
        'description' => 'Boolean value for setting presense in module view',
        'type' => 'int',
        'size' => 'tiny',
        'default' => 1,
        'not null' => TRUE,),
      'created_at' => array(
        'description' => 'Date when record was created.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE,),
    ),
    'unique_keys' => array(
      'caid' => array('caid'),
    ),
    'primary key' => array('caid'),
  );

  $schema['clantastic_rank'] = array(
    'description' => 'Table to establish rank types',
    'fields' => array(
      'crid' => array(
        'description' => 'The primary identifier for a clan rank',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'title' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',),
      'description' => array(
        'type' => 'varchar',
        'length' => 512,
        'not null' => TRUE,
        'default' => '',),
      'fid' => array(
        'description' => 'Graphic image associated with this rank.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'priority' => array(
        'description' => 'Used for sorting purposes',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 999,),
      'is_active' => array(
        'description' => 'Boolean value for setting presense in module view',
        'type' => 'int',
        'size' => 'tiny',
        'default' => 1,
        'not null' => TRUE,),
      'created_at' => array(
        'description' => 'Date when record was created.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE,),
    ),
    'unique_keys' => array(
      'crid' => array('crid'),
    ),
    'primary key' => array('crid'),
  );

  $schema['clantastic_award'] = array(
    'description' => 'Table to establish rank types',
    'fields' => array(
      'cwid' => array(
        'description' => 'The primary identifier this award or accomidation',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'title' => array(
        'type' => 'varchar',
        'length' => 512,
        'not null' => TRUE,
        'default' => '',),
      'description' => array(
        'type' => 'varchar',
        'length' => 2048,
        'not null' => TRUE,
        'default' => '',),
      'fid' => array(
        'description' => 'Graphic image associated with this award.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'priority' => array(
        'description' => 'Used for sorting purposes',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 999,),
      'is_active' => array(
        'description' => 'Boolean value for setting presense in module view',
        'type' => 'int',
        'size' => 'tiny',
        'default' => 1,
        'not null' => TRUE,),
      'created_at' => array(
        'description' => 'Date when record was created.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE,),
    ),
    'unique_keys' => array(
      'cwid' => array('cwid'),
    ),
    'primary key' => array('cwid'),
  );


  // Clan Member - Clan Award join table.
  $schema['clantastic_award_clantastic_member'] = array(
    'description' => 'Table to establish relationship between clan members and clan awards',
    'fields' => array(
      'cwcmid' => array(
        'description' => 'Unique identifier for this award assigned to a member',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'cwid' => array(
        'description' => 'The primary identifier for an award or decoration',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'cid' => array(
        'description' => 'The primary identifier for a clan member',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,),
      'comments' => array(
        'type' => 'varchar',
        'length' => 1024,
        'not null' => TRUE,
        'default' => '',),
      'created_at' => array(
        'description' => 'Date when record was created.',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => TRUE,),
    ),
    'indexes' => array(
      'cwid' => array('cwid'),
      'cid' => array('cid'),
    ),
    'primary key' => array('cwcmid'),
  );
  return $schema;
}


/**
 * Implements hook_install().
 */
function clantastic_install() {
  drupal_install_schema('clantastic_config');

  clantastic_set_default_config();
  clantastic_load_default_images();
  clantastic_load_default_ranks();
  clantastic_load_default_awards();

}


/**
 * Set default to newly created config file.
 */
function clantastic_set_default_config() {
  // Add a config record.
  $table = 'clantastic_config';
  $record = new stdClass();
  $record->is_ranked_displayed = 1;
  $record->is_fullname_displayed = 1;
  $record->is_location_displayed = 1;
  $record->is_active = 1;
  $record->updated_at = time();
  drupal_write_record($table, $record);

}


/**
 * Load default images from included files.
 */
function clantastic_load_default_images() {
  // Get an inventory of images to copy.
  $file_root = drupal_get_path('module', 'clantastic');
  $f = $file_root . '/images';
  $files = array();
  $files = file_scan_directory($f, '/.*\.png$/');

  $file_directory = 'public://clantastic/images';
  file_prepare_directory($file_directory, FILE_CREATE_DIRECTORY);

  foreach ($files as $fn) {
    $fn->uid = 1;
    $fn->status = 1;
    $fn->filemime = file_get_mimetype($fn->uri);
    $image = file_copy($fn, $file_directory, FILE_EXISTS_REPLACE);
  }
}


/**
 * Load dummy members.
 */
function clantastic_load_dummy_members() {
  $file_root = drupal_get_path('module', 'clantastic');
  $file_name = '/test_files/dummy_members.txt';
  $file_path = $file_root . $file_name;

  $members = file($file_path);

  // Add a record.
  $table = 'clantastic_member';

  foreach ($members as $member) {
    list($uid, $rid, $membership_at) = explode("|", $member);

    $record = new stdClass();
    $record->uid = $uid;
    $record->crid = $rid;
    $record->membership_at = $membership_at;
    $record->created_at = time();
    $record->is_active = TRUE;
    drupal_write_record($table, $record);
  }
}


/**
 * Load dummy member awards.
 */
function clantastic_load_dummy_awards() {
  $file_root = drupal_get_path('module', 'clantastic');
  $file_name = '/test_files/dummy_awards.txt';
  $file_path = $file_root . $file_name;

  $awards = file($file_path);

  // Add a record.
  $table = 'clantastic_member';

  foreach ($awards as $award) {
    list($cwid, $cid, $comment) = explode("|", $award);

    $record = new stdClass();
    $record->cwid = $cwid;
    $record->cid = $cid;
    $record->created_at = time();
    $record->comments = $comment;
    drupal_write_record($table, $record);
  }
}


/**
 * Create Default Ranks from a configuation file.
 */
function clantastic_load_default_ranks() {
  // Read rank defaults file.
  $file_root = drupal_get_path('module', 'clantastic');
  $file_name = '/test_files/rank_defaults.txt';
  $file_path = $file_root . $file_name;

  $ranks = file($file_path);

  // Add a record.
  $table = 'clantastic_rank';

  foreach ($ranks as $rank_num => $rank) {
    list($rank_title, $image_name, $priority, $description) = explode("|", $rank);

    // Locate image id.
    $fid = clantastic_locate_image_id($image_name);

    $record = new stdClass();
    $record->title = $rank_title;
    $record->description = $description;
    $record->created_at = time();
    $record->fid = $fid;
    $record->priority = $priority;
    drupal_write_record($table, $record);
  }
}


/**
 * Create Default Awards from a configuation file.
 */
function clantastic_load_default_awards() {
  // Read awards defaults file.
  $file_root = drupal_get_path('module', 'clantastic');
  $file_name = '/test_files/award_defaults.txt';
  $file_path = $file_root . $file_name;

  $awards = file($file_path);

  // Add a config record.
  $table = 'clantastic_award';

  foreach ($awards as $award_num => $award) {
    list($award_title, $priority, $image_name, $description) = explode("|", $award);

    // Locate image id.
    $fid = clantastic_locate_image_id($image_name);

    $record = new stdClass();
    $record->title = $award_title;
    $record->priority = $priority;
    $record->description = $description;
    $record->created_at = time();
    $record->fid = $fid;
    drupal_write_record($table, $record);
  }
}


/**
 * Retrieve an image definition.
 *
 * @params $image_name
 *   An image object.
 *
 * @return Integer.
 *   The id of the submitted image.
 */
function clantastic_locate_image_id($image_name) {
  $fid = db_query("SELECT fid FROM {file_managed} WHERE filename = :image_name", array(':image_name' => $image_name))->fetchField();
  return $fid;
}
