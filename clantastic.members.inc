<?php

/**
 * @file
 * Member related lists and forms
 */

/**
 * include common functions
 */
module_load_include('inc', 'clantastic', '/includes/custom_functions');


/**
 * Display edit form for member definition.
 *
 * @params $form
 *   Base input form.
 * @params $form_state
 *   Input form updated by user.
 * @params $cid
 *   Clan member id
 *
 * @return: Object
 *   A $form object
 */
function clantastic_member_detail_edit($form, &$form_state, $cid) {
  // Get memeber details.
  $member = clantastic_get_member($cid);

  // Build a pretty container.
  $form['member'] = array(
    '#title' => $member->name,
    '#type' => 'fieldset',
  );

  // Hidden field for CID.
  $form['member']['cid'] = array(
    '#type' => 'hidden',
    '#title' => t('ID'),
    '#value' => $member->cid,
  );

  // Rank.
  $rank_options = clantastic_get_rank_options();
  $form['member']['crid'] = array(
    '#type' => 'select',
    '#title' => t('Rank'),
    '#options' => $rank_options,
    '#value' => $member->crid,
  );

  // Membership Status.
  $form['member']['is_active'] = array(
    '#type' => 'select',
    '#title' => t('Membership Status'),
    '#options' => array(0 => t('Inactive'), 1 => t('Active')),
    '#value' => $member->is_active,
  );

  // Section Header.
  $form['member']['section_title'] = array(
    '#type' => 'item',
    '#title' => t('Awards & Decorations'),
  );

  // Member Awards.
  $header = array(
    'image' => t('Pin'),
    'name' => t('Name'),
    'comments' => t('Comments'),
  );

  $query = db_query('
    SELECT cacm.cid, cacm.cwid, cacm.comments, cacm.cwcmid, ca.title, ca.fid, ca.description
    FROM clantastic_award ca
    JOIN clantastic_award_clantastic_member cacm ON cacm.cwid = ca.cwid
    WHERE cacm.cid = 3
    ORDER BY ca.priority
  ');
  $awards = $query->fetchAll();

  $options = array();
  foreach ($awards as $award) {
    $options[$award->cwcmid] = array(
      'image' => theme_image(clantastic_get_award_image_attributes($member, $award)),
      'name' => $award->title,
      'comments' => $award->comments,
    );
    // Set checkbox value.
    $default_value[$award->cwcmid] = TRUE;
  }

  $form['member']['cwcmid'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No Awards or Decorations assigned to this member.'),
    '#default_value' => $default_value,
  );

  $form['member']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}


/**
 * Process edits to clan member definition.
 */
function clantastic_member_detail_edit_submit($form, &$form_state) {
  $value_change = FALSE;
  $cid = $form_state['input']['cid'];

  // Create an array of values we'll validate.
  $check_field_names = array('crid', 'is_active', 'cwcmid');

  foreach ($check_field_names as $check_field) {
    $updated_value = $form_state['input'][$check_field];
    $original_value = $form_state['values'][$check_field];

    if ($check_field == 'cwcmid') {
      // Remove any unselected awards from this member.
      foreach ($updated_value as $checkbox_index => $checkbox) {
        if (!isset($checkbox)) {
          $value_change = TRUE;
          clantastic_remove_member_award_by_cwcmid($checkbox_index);
        }
      }
    }
    elseif ($updated_value != $original_value) {
      $value_change = TRUE;
      clantastic_update_clantastic_member_row($cid, $check_field, $updated_value);
    }
  }

  if ($value_change) {
    $status_message = t('Member Profile Updated');
    drupal_set_message(check_plain($status_message));
  }
}


/**
 * Update a clantastic_member table row.
 *
 * @params $cid
 *   Clan member ID.
 * @params $field_name
 *   The row field name to be updated.
 * @params $updated_value
 *   The new value.
 *
 * @return Object
 *   Return the database object created by the db_update function.
 */
function clantastic_update_clantastic_member_row($cid, $field_name, $updated_value) {
  $return_value = db_update('clantastic_member')
  ->fields(array($field_name => $updated_value))
  ->condition('cid', $cid, '=')
  ->execute();
  return $return_value;
}


/**
 * Retrieve member definition form.
 */
function clantastic_member_detail($uid = NULL) {
  if ($uid === NULL) {
    global $user;
    $uid = $user->uid;
  }

  $form = clantastic_build_member_form($uid);

  return $form;
}


/**
 * Build member definition form.
 */
function clantastic_build_member_form($cid) {
  $member = clantastic_get_member($cid);

  // Insignia and Name for a title.
  $rank = clantastic_get_rank_image_attributes($member);
  $title_string = $member->name;
  $form['member'] = array(
    '#title' => check_plain($title_string) . ' ' . theme_image($rank),
    '#type' => 'fieldset',
  );

  // Rank.
  $description = '(' . $member->description . ')';
  $form['member']['rank'] = array(
    '#type' => 'item',
    '#title' => t('Rank'),
    '#markup' => $member->title,
    '#description' => check_plain($description),
  );

  // Membership Status.
  $form['member']['status'] = array(
    '#type' => 'item',
    '#title' => t('Membership Status'),
    '#markup' => $member->is_active ? 'Active' : 'Inactive',
  );

  // Section Header.
  $form['member']['section_title'] = array(
    '#type' => 'item',
    '#title' => t('Awards & Decorations'),
  );

  // Member Awards.
  $header = array(
    array('data' => t('Pin'), 'width' => '10%'),
    array('data' => t('Title'), 'width' => '25%'),
    array('data' => t('Comments')),
  );

  $rows = clantastic_get_table_formatted_granted_awards($member);

  $form['member']['table'] = array(
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No Awards or Decorations assigned to this member.'),
  );

  return $form;
}


/**
 * Label for the form.
 */
function clantastic_clantastic_info() {
  return t('information');
}


/**
 * Label for the form.
 */
function clantastic_member_detail_title() {
  return t('Member Details');
}


/**
 * Get a formatted row of data represeting a member.
 */
function clantastic_get_table_formatted_granted_awards($member) {
  $awards = clantastic_granted_awards($member);
  foreach ($awards as $award) {
    $value[] = array(
      'data' => array(
        '#image' => theme_image(clantastic_get_award_image_attributes($member, $award)),
        '#name' => $award->title,
        '#comment' => $award->comments,
      ),
    );
  }
  return $value;
}


/**
 * Create an array of objects that represent all awards assigned to a member.
 */
function clantastic_granted_awards($member) {
  $value = "";
  $query = db_select('clantastic_member', 'c');
  $query->leftJoin('clantastic_award_clantastic_member', 'cacm', 'cacm.cid = c.cid');
  $query->leftJoin('clantastic_award', 'ca', 'ca.cwid = cacm.cwid');
  $query
  ->fields('ca', array('title', 'description', 'fid', 'priority', 'cwid'))
  ->fields('cacm', array('cwcmid', 'cwid', 'comments'))
  ->condition('c.cid', $member->cid, '=')
  ->orderBy('priority', 'ASC');
  $awards = $query->execute();
  return $awards;
}

/**
 * Assemble an object that includes enough to create an image.
 *
 * @params $member
 *   An object that contains member info.
 * @params $award
 *   An object that contains award info.
 *
 * @return Object
 *   Object that contains an image defintion.
 */
function clantastic_get_award_image_attributes($member, $award) {
  $local_image = db_query("SELECT * FROM {file_managed} WHERE fid = :fid", array(':fid' => $award->fid))->fetchObject();

  $variables = array(
    'path' => clantastic_issetor($local_image->uri, 'public://clantastic/images/r0.png'),
    'alt' => $award->title,
    'title' => $award->description,
    'attributes' => array('class' => 'some-img', 'id' => 'my-img'),
  );

  return $variables;
}


/**
 * Assemble an object that includes enough to create an image.
 */
function clantastic_get_rank_image_attributes($member) {
  $local_image = db_query("SELECT * FROM {file_managed} WHERE fid = :fid", array(':fid' => $member->fid))->fetchObject();

  $variables = array(
    'path' => clantastic_issetor($local_image->uri, 'public://clantastic/images/r0.png'),
    'alt' => $member->title,
    'title' => $member->title,
    'attributes' => array(
      'class' => 'clantastic-rank-image',
      'id' => 'my-img',
    ),
  );

  return $variables;
}


/**
 * Locate and create an object of all member information.
 */
function clantastic_get_member($uid) {
  $query = db_select('users', 'u');
  $query->leftJoin('clantastic_member', 'c', 'u.uid = c.uid');
  $query->leftJoin('clantastic_rank', 'cr', 'cr.crid = c.crid');
  $result = $query
  ->fields('u', array('name', 'mail', 'created', 'uid'))
  ->fields('c', array('cid', 'crid', 'is_active', 'membership_at'))
  ->fields('cr', array('crid', 'title', 'fid', 'description'))
  ->condition('u.uid', $uid, '=')
  ->execute();
  $member = $result->fetchObject();

  return $member;
}


/**
 * Create and display a list of members.
 */
function clantastic_member_list() {
  drupal_set_title(t(''));
  $form = array();

  $header = array(
    array('data' => t('Name'), 'width' => '15%'),
    array('data' => t('Rank'), 'width' => '12%'),
    array('data' => t('Awards & Decorations')),
  );

  $query = db_select('users', 'u');
  $query->leftJoin('clantastic_member', 'c', 'u.uid = c.uid');
  $query->leftJoin('clantastic_rank', 'cr', 'cr.crid = c.crid');
  $query
  ->extend('TableSort')
  ->fields('u', array('name', 'mail', 'created', 'uid'))
  ->fields('c', array('cid', 'crid', 'is_active'))
  ->fields('cr', array('crid', 'title', 'fid', 'priority'))
  ->condition('u.uid', 1, '>')
  ->condition('c.is_active', 1, '=')
  ->orderBy('cr.priority', 'ASC')
  ->orderBy('name', 'ASC');

  $users = $query->execute();

  $rows = array();

  foreach ($users as $user) {
    $result = db_query("SELECT * FROM {file_managed} WHERE fid = :fid", array(':fid' => $user->fid));
    $local_image = $result->fetchObject();

    $variables = array(
      'path' => clantastic_issetor($local_image->uri, 'public://clantastic/images/r0.png'),
      'alt' => $user->title,
      'title' => $user->title,
      'attributes' => array('class' => 'clantastic-rank-image', 'id' => 'my-img'),
    );

    $rows[] = array(
      'data' => array(
        l($user->name, 'clantastic/details/' . $user->uid),
        theme_image($variables),
        clantastic_awards($user),
      ),
    );
  }

  $form['table'] = array(
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No users found'),
  );

  return $form;
}


/**
 * Create and display a list of members.
 */
function clantastic_roster() {
  drupal_set_title(t(''));

  $form = array();
  $header = array(
    'name' => t('Name'),
    'rank' => t('Rank'),
    'awards_list' => t('Awards and Decorations'),
  );

  $query = db_select('users', 'u');
  $query->leftJoin('clantastic_member', 'c', 'u.uid = c.uid');
  $query->leftJoin('clantastic_rank', 'cr', 'cr.crid = c.crid');
  $query
  ->extend('TableSort')
  ->fields('u', array('name', 'mail', 'created', 'uid'))
  ->fields('c', array('cid', 'crid', 'is_active'))
  ->fields('cr', array('crid', 'title', 'fid', 'priority'))
  ->condition('u.uid', 2, '>=')
  ->orderBy('cr.priority', 'ASC')
  ->orderBy('u.name', 'ASC');

  $users = $query->execute();

  $options = array();

  foreach ($users as $member_index => $user) {
    $result = db_query("SELECT * FROM {file_managed} WHERE fid = :fid", array(':fid' => $user->fid));
    $local_image = $result->fetchObject();

    $variables = array(
      'path' => clantastic_issetor($local_image->uri, 'public://clantastic/images/r0.png'),
      'alt' => $user->title,
      'title' => $user->title,
      'attributes' => array('class' => 'clantastic-rank-image', 'id' => 'my-img'),
    );

    // Set checkbox value.
    $default_value[$user->uid] = TRUE;
    if (!isset($user->cid)) {
      $default_value[$user->uid] = FALSE;
    }
    elseif ($user->is_active == 0) {
      $default_value[$user->uid] = FALSE;
    }

    $options[$user->uid] = array(
      'name' => isset($user->cid) ? l($user->name, 'clantastic/member/edit/' . $user->uid) : $user->name,
      'rank' => $user->is_active ? theme_image($variables) : '',
      'awards_list' => $user->is_active ? clantastic_awards($user) : '',
    );

  }

  $form['member'] = array(
    '#title' => t('List of all registered users.'),
    '#type' => 'fieldset',
    '#description' => t('Select the users to include in the Clan Roster'),
  );

  $form['member']['table'] = array(
    '#type' => 'tableselect',
    '#multiple' => TRUE,
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No users found'),
    '#default_value' => $default_value,

  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save List'));
  $form['#submit'][] = 'clantastic_member_submit';

  return $form;
}


/**
 * Update member settings values if changed at submission.
 */
function clantastic_member_submit($form, &$form_state) {
  $list_values = $form_state['values']['table'];
  $list = implode(",", array_values($list_values));
  watchdog('clantastic', 'form data:  ' . print_r($list, TRUE));

  // Remove members not in list.
  $table = 'clantastic_member';

  $results = db_select($table, 'c')
  ->fields('c')
  ->condition('c.uid', array_values($list_values), 'NOT IN')
  ->execute();

  foreach ($results as $update_member) {
    watchdog('clantastic', 'make inactive:  ' . print_r($update_member, TRUE));
    db_update($table)
    ->fields(array('is_active' => 0))
     ->condition('uid', $update_member->uid, '=')
     ->execute();
  }

  // Activate any existing members.
  $results = db_select($table, 'c')
  ->fields('c')
  ->condition('c.uid', array_values($list_values), 'IN')
  ->condition('c.is_active', 0, '=')
  ->execute();

  foreach ($results as $update_member) {
    db_update($table)
    ->fields(array('is_active' => 1))
     ->condition('uid', $update_member->uid, '=')
     ->execute();
  }

  // Add new memebers from list.
  foreach (array_values($list_values) as $uid) {
    $count_query = db_select($table, 'c')
    ->condition('c.uid', $uid, '=')
    ->countQuery()
    ->execute()
    ->fetchField();

    if ($count_query == 0 && $uid <> 0) {
      clantastic_create_member($uid);
    }
  }
}


/**
 * Create a clan member.
 */
function clantastic_create_member($uid) {
  $table = 'clantastic_member';
  $record = new stdClass();
  $record->uid = $uid;
  $record->crid = clantastic_fetch_config_field('default_rank_id');
  $record->is_active = 1;
  $record->created_at = time();
  drupal_write_record($table, $record);
}
