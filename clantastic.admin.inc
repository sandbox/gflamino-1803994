<?php
/**
 * @file
 * Configure Clantastic
 */

/**
 * include common functions
 */
module_load_include('inc', 'clantastic', '/includes/custom_functions');


/**
 * Display various config options.
 *
 * @return Array.
 *   A form array.
 */
function clantastic_admin_settings() {

  $cfg_display_rank = db_query("SELECT is_rank_displayed FROM {clantastic_config}")->fetchField();
  $cfg_display_fullname = db_query("SELECT is_fullname_displayed FROM {clantastic_config}")->fetchField();
  $cfg_display_location = db_query("SELECT is_location_displayed FROM {clantastic_config}")->fetchField();
  $cfg_display_details = db_query("SELECT is_active FROM {clantastic_config}")->fetchField();

  // Section Header.
  $form['section_title'] = array(
    '#type' => 'item',
    '#title' => t('Display Settings'),
  );

  $form['display_rank'] = array(
    '#type' => 'checkbox',
    '#title' => 'Display Rank',
    '#default_value' => $cfg_display_rank,
    '#description' => t("Display the Clan Member's rank in the summary list view"),
  );

  $form['display_fullname'] = array(
    '#type' => 'checkbox',
    '#title' => 'Display Full Name',
    '#default_value' => $cfg_display_fullname,
    '#description' => t("Display the Clan Member's full name in the summary list view"),
  );

  $form['display_location'] = array(
    '#type' => 'checkbox',
    '#title' => 'Display Location',
    '#default_value' => $cfg_display_location,
    '#description' => t("Display the Clan Member's location in the summary list view"),
  );

  $form['display_details'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show Details Link',
    '#default_value' => $cfg_display_details,
    '#description' => t("Display the option for users to see the Clan Member's details"),
  );

  $form['default_rank'] = array(
    '#type' => 'select',
    '#title' => t('Default Rank'),
    '#options' => clantastic_get_rank_options(),
    '#default_value' => clantastic_fetch_config_field('default_rank_id'),
  );

  $form['#submit'][] = 'clantastic_admin_settings_submit';
  return system_settings_form($form);

}


/**
 * Updated clan settings values if changed at submission.
 *
 * @params $form_state
 *   Input form.
 * @params $form_name
 *    Source input form object's name.
 */
function clantastic_admin_settings_submit($form, &$form_state) {
  // Display Rank.
  $field_name = 'is_rank_displayed';
  $form_name = 'display_rank';
  clantastic_update_config_if_different($form_state, $field_name, $form_name);

  // Display Full Name.
  $field_name = 'is_fullname_displayed';
  $form_name = 'display_fullname';
  clantastic_update_config_if_different($form_state, $field_name, $form_name);

  // Display Location.
  $field_name = 'is_location_displayed';
  $form_name = 'display_location';
  clantastic_update_config_if_different($form_state, $field_name, $form_name);

  // Display Details Link.
  $field_name = 'is_active';
  $form_name = 'display_details';
  clantastic_update_config_if_different($form_state, $field_name, $form_name);

  // Default Rank Name.
  $field_name = 'default_rank_id';
  $form_name = 'default_rank';
  clantastic_update_config_if_different($form_state, $field_name, $form_name);
}


/**
 * Compare value from input form against the stored db value, update different.
 *
 * @params $field_name
 *   A string value representing the column name to return.
 * @params $form_state
 *   Input form.
 * @params $form_name
 *    Source input form object's name.
 *
 * @return Boolean.
 *   True on successful updates.
 */
function clantastic_update_config_if_different($form_state, $field_name, $form_name) {
  $return_value = FALSE;
  $db_value = clantastic_fetch_config_field($field_name);
  $updated_value = $form_state['values'][$form_name];
  if ($updated_value != $db_value) {
    $return_value = db_update('clantastic_config')
    ->fields(array($field_name => $updated_value))
    ->condition('ccid', 1, '=')
    ->execute();
  }
  return $return_value;
}
