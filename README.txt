-- CONTENTS OF THIS FILE --


 * Summary
 * Installation
 * Configuration
 * File format advice and preloaded definitions
 * Contact Information


-- SUMMARY --


A simple module for creating a relationship between your site's users and their
clan affiliation. Information related to when the user entered the clan, his/her
(a military like) 'rank', awards and accommodations (think pins, badges, or
medals).

In concept we assign a 'rank' to a user (i.e. 'Private') and award a medal for
participation in a certain activity (i.e. 'North Africa Campaign').

The module has two major view types.

1. Maintenance.  The maintenance view exposes the ability to create, modify, and
   delete 'ranks' and 'awards'.  Both rank and award definitions contain a name,
   description, and an (optional) image.  Award definition contains a priority
   value field for sorting purposes.
2. Clan Member.  A list view to display all users that have been assigned a rank
   that includes the 'awards' graphics.  And a user detail view that provides
   details of the user.


-- INSTALLATION --


This should outline a step-by-step explanation of how to install the module.

1. Download the module.
2. Upload the [Drupal Installation folder]/sites/all/modules folder.
3. Assign user permissions (admin/people/permissions#module-clantastic)


-- CONFIGURATION --


Upon installation a new Clan Roster link will be visible in the Navigation menu.

1. As a module administrator, select the registered Drupal user to include in 
   the Clan Roster (Navigation Menu > Clan Roster > Edit Roster)
2. Define Ranks and Awards (Navigation Menu > Clan Roster > Rank Maintenance or
   > Award Maintenance)
3. Assign Ranks and Awards to individual Clan Roster members (Navigation Menu >
   Clan Roster > Mass Rank Assign or > Mass Award Assign)


-- Files --

Graphic Files Advice

Award Images: Optimal image file dimensions are 55px by 16px.
Rank Images: Optimal image file dimensions are 16px by 16px.

Preloaded Definition

Several ranks and awards are loaded at installation.  Feel free to delete these
definitions.  Note, you cannot delete the 'default' rank before you designate an
alternative in Config settings (admin/config/clan_rank_and_decorations/settings)

-- CONTACT --


Current Maintainers:

Gilbert Flamino (gflamino) - http://drupal.org/user/2254650


This project has been sponsored by:

* NEWNETWORK.CC, LLC.
  Our core principles lie in simplicity, beautiful design, and brilliant 
  engineering. We believe in less features and better functionality that
  fuses seamlessly with a stunning and simple interface. Our goal is not
  only to build the best products that we can, but to also make a large
  impact on every project we work on. At newnetwork.cc, we do things
  differently.
